import inspect
from textwrap import dedent

from django.db import models

import dirtyfields
from dirtyfields import DirtyFieldsMixin
from dirtyfields.compare import raw_compare, compare_states, normalise_value
from dirtyfields.compat import is_buffer
from django.db.models.signals import pre_save

from copy import deepcopy

from django.core.exceptions import ValidationError
from django.db.models.expressions import BaseExpression
from django.db.models.expressions import Combinable
from django.db.models.signals import post_save, m2m_changed


# TODO clarify patching imports
def patch_as_dict():
    source_code = dedent(inspect.getsource(DirtyFieldsMixin._as_dict))
    source_code = source_code.replace(
        'FIELDS_TO_CHECK',
        'EVENT_FIELDS_TO_CHECK',
    ).replace('_as_dict', '_as_dict_event')
    result = {}
    exec(compile(source_code, '<string>', 'exec'), globals(), result)
    # TODO print has to be removed
    print('patch _as_dict')
    # DirtyFieldsEventMixin._as_dict_event = result['_as_dict_event']
    return result['_as_dict_event']


def patch_get_dirty_fields():
    source_code = dedent(inspect.getsource(DirtyFieldsMixin.get_dirty_fields))
    source_code = source_code.replace('_as_dict', '_as_dict_event')
    result = {}
    exec(compile(source_code, '<string>', 'exec'), globals(), result)
    return result['get_dirty_fields']


class DirtyFieldsEventMixin(DirtyFieldsMixin):
    EVENT_FIELDS_TO_CHECK = []
    EVENT_MODEL_CLASS = None
    EVENT_DATA_FIELD = []
    # to treat field as a dict and select only desired keys
    EVENT_DICT_FIELD = {}
    # to get specific compare and normalize functions
    # override attributes *_function from DirtyFieldsMixin
    # see also
    # https://github.com/romgar/django-dirtyfields/blob/develop/src/dirtyfields/compare.py

    _as_dict_event = patch_as_dict()
    get_dirty_fields_event = patch_get_dirty_fields()

    def __init__(self, *args, **kwargs):
        # keep orignal behaviour from DirtyFields
        # see FIELDS_TO_CHECK in DirtyFieldsMixin
        FIELDS_TO_CHECK = getattr(self, 'FIELDS_TO_CHECK', [])
        if FIELDS_TO_CHECK is None:
            FIELDS_TO_CHECK = []
        print('FIELDS_TO_CHECK', FIELDS_TO_CHECK)
        self.FIELDS_TO_CHECK = list(
            set(self.EVENT_FIELDS_TO_CHECK) | set(FIELDS_TO_CHECK))
        super(DirtyFieldsEventMixin, self).__init__(*args, **kwargs)
        sender = self.__class__
        pre_save.connect(
            create_event_object,
            sender=sender,
            dispatch_uid=f'{sender.__name__}-DirtyFieldsEventMixin-create',
        )


def create_event_object(sender, instance, **kwargs):
    ''' Create object for changed fields
    '''
    EVENT_FIELDS_TO_CHECK = sender.EVENT_FIELDS_TO_CHECK
    EVENT_DICT_FIELD = sender.EVENT_DICT_FIELD
    EVENT_DATA_FIELD = sender.EVENT_DATA_FIELD
    EVENT_MODEL_CLASS = sender.EVENT_MODEL_CLASS

    compare, kwargs = sender.compare_function

    model_name = sender.__name__ + '.'

    def create_object(name, value):
        print('name :', model_name + name, '; value :', str(value))
        if isinstance(value, models.Model):
            value = value.id
            print('IS OBJECT')
        EVENT_MODEL_CLASS.objects.create(
            name=model_name + name,
            value=value,
        )

    # for key in instance.get_dirty_fields_event():
    #     value = getattr(instance, key)
    #     print('VAL FIELD', value)
    #     if key in EVENT_DICT_FIELD:
    #         # if (key in EVENT_DICT_FIELD
    #         #         and type(value).__name__ in EVENT_DATA_FIELD):
    #         current_value = getattr(instance, key, {})
    #         print(key, value)
    #         name_key = key + '.'
    #         dict_keys = EVENT_DICT_FIELD[key] or next(zip(*value.NATURES))
    #         for d_key in value:
    #             if value.nature not in dict_keys:
    #                 continue
    #             if d_key not in current_value:
    #                 create_object(name=name_key + d_key, value=value[d_key])
    #             elif not compare(current_value[d_key], value[d_key], kwargs):
    #                 create_object(name=name_key + d_key, value=value[d_key])
    #     else:
    #         create_object(name=key, value=value)

# class FlElement(Streamable, DirtyFieldsEventMixin):
    # DirtyFieldsEventMixin
    # EVENT_FIELDS_TO_CHECK = ['custom_fields']
    # EVENT_FIELDS_TO_CHECK = [
    #     'position',
    #     'custom_fields',
    # ]
    # EVENT_MODEL_CLASS = Event

    # to treat field as a dict and select only desired keys
    # EVENT_DATA_FIELD = [
    #     CustomField.__name__,
    # ]
    # EVENT_DICT_FIELD = {
    #     'custom_fields': [
    #         'person',
    #         'organization',
    #     ],
    # }
