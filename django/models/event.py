from django.db import models
from django.core.exceptions import ValidationError
from django.dispatch import receiver
from django.db.models.signals import pre_save
from django.contrib.contenttypes.fields import GenericForeignKey, GenericRelation
from django.contrib.contenttypes.models import ContentType


class EventManager(models.Manager):
    def create(self, *args, **kwargs):
        # automatically assign self.value to the right nature
        if 'nature' not in kwargs:
            if 'value' not in kwargs:
                raise ValidationError(
                    'Event requires at least nature or value fields')
            value = kwargs.pop('value')
            value_type_name = type(value).__name__
            # TODO performances : do not call zip on every save
            if value_type_name not in next(zip(*Event.VALUE_NATURES)):
                if isinstance(value, models.Model):
                    value_type_name = 'model'
                else:
                    raise ValidationError(
                        f"Event's value's type is not supported : {value_type_name}"
                    )
            kwargs['nature'] = value_type_name
            kwargs[value_type_name + '_value'] = value
        # check if a pair nature/value is well assigned
        if ('nature' not in kwargs
                or not kwargs['nature'] + '_value' in kwargs):
            raise ValidationError(
                'Event has no value matching with his nature')
        super().create(*args, **kwargs)


# class EventRelationship(models.Model):
#     model_type = models.ForeignKey(
#         ContentType,
#         blank=True,
#         null=True,
#         on_delete=models.SET_NULL,
#     )
#     object_id = models.PositiveIntegerField(blank=True, null=True)
#     model_value = GenericForeignKey('model_name', 'object_id')


class Event(models.Model):
    objects = EventManager()

    name = models.CharField(max_length=64, blank=False)

    str_value = models.CharField(max_length=128, blank=True)
    int_value = models.IntegerField(blank=True, null=True)
    # TODO max_digits ? decimal_places ?
    float_value = models.DecimalField(
        max_digits=10,
        decimal_places=5,
        blank=True,
        null=True,
    )
    date_value = models.DateField(blank=True, null=True)
    datetime_value = models.DateTimeField(blank=True, null=True)

    # relationships = GenericRelation(
    #     'EventRelationship',
    #     content_type_field='model_name',
    #     object_id_field='object_id',
    # )
    fl_element = models.ForeignKey(
        'FlElement',
        related_name='events',
        on_delete=models.PROTECT,
        blank=True,
        null=True,
    )

    custom_field = models.ForeignKey(
        'CustomField',
        related_name='events',
        on_delete=models.PROTECT,
        blank=True,
        null=True,
    )

    model_name = models.ForeignKey(
        ContentType,
        blank=True,
        null=True,
        on_delete=models.SET_NULL,
    )
    object_id = models.PositiveIntegerField(blank=True, null=True)
    model_value = GenericForeignKey('model_name', 'object_id')

    STR = 'str'
    INT = 'int'
    FLOAT = 'float'
    DATE = 'date'
    DATETIME = 'datetime'
    MODEL = 'model'

    VALUE_NATURES = [
        (STR, 'String value'),
        (INT, 'Integer value'),
        (FLOAT, 'Decimal value'),
        (DATE, 'Date value'),
        (DATETIME, 'Datetime value'),
        (MODEL, 'Relationship value'),
    ]
    # TODO VALUE_NATURES_TYPES next(zip()) ?
    nature = models.CharField(
        max_length=20,
        choices=VALUE_NATURES,
        blank=False,
    )

    created_at = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return '{} : {}'.format(self.name, self.nature, self.get_value())

    def get_value(self):
        return getattr(self, self.nature + '_value')
